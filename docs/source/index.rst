.. Elevate documentation master file.
Welcome to Elevate!
===================================

.. toctree::
   
   path_finding/*
   Server/*

What is Elevate?
--------------------------------------
Elevate is a fitness application that allows bikers and runners to find cycles that meet their elevation and distance goals. For example, Elevate can help you find a cycle that begins and ends at your house that is flat, or maybe has mellow inclines, or avoids so-called “heartbreak hills”.

Why Elevate?
------------------
We built Elevate to solve a problem we had ourselves.
When runners and cyclists need to find a new exercise route they’re faced with
a difficult and time consuming problem.
Unfortunately, currently available mapping apps don’t allow
users to search for circuit routes or control for elevation change. Athletes
either need to find a circuit someone else has planned and published or take on
the task of mapping one on their own. Elevate is the solution. Elevate finds
circuit routes for users that match their desired elevation and distance goals.


Screenshots
-------------
.. figure:: ../../imgs/ElevateSplashScreen.png
   :scale: 1

   Elevate's splash page, centered at the user's location. Possible profiles and options displayed on the left.



.. figure:: ../../imgs/ElevateRouteScreen.png
   :scale: 1

   An example of a circuit found by Elevate.



Documentation
----------------------
The Elevate documentation can be found `here <https://bryce-bg.gitlab.io/elena-elevation-based-navigation/docs/build/html/index.html>`_.
These links may help you navigate the documentation,

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Project Structure
----------------------
Elevate’s code-base is organized into three sections,

* **path-finding**, found in the `/path_finding <https://gitlab.com/Bryce-BG/elena-elevation-based-navigation/tree/master/path_finding>`_ directory
* **server**,  found in the `/Server <https://gitlab.com/Bryce-BG/elena-elevation-based-navigation/tree/master/Server>`_ directory
* **tests**, found in the `/tests <https://gitlab.com/Bryce-BG/elena-elevation-based-navigation/tree/master/tests>`_ directory

Setting Up the Application
----------------------------
All dependencies are stored in `requirements.yml`. Even with Anaconda, these dependencies may be difficult to install. We recommend installing this environment on a Unix-based system.

You can install and run the environment with these commands:

.. code:: bash

    conda env create -f environment.yml
    conda activate ox
    cd Server
    python Server.py

Note: in order to download new graphs you must have a google maps API key in ``path_finding/google_maps_api_key.txt``. Feel free to ask Julian (joks at umass) for his API key. There are a few saved graphs that can be used without downloading map data from an API, for example, you can use this location (42.394982, -72.527046) to download a graph that contains the UMass Amherst campus.


How the Algorithm Works
----------------------------------
The solver algorithm is implemented `here <https://gitlab.com/Bryce-BG/elena-elevation-based-navigation/blob/master/path_finding/solver.py>`_.
We use a beam search algorithm to efficiently find a cycle whose distance / altitude profile best matches the user’s desired profile. We believe a beam search algorithm is a good choice because it is an anytime algorithm that performs well on a wide range of graphs and objectives.
The algorithm proceeds in 2 stages:

1. Initialization: we initialize the population by finding random cycles in the graph that start at the specified starting locations and meet the total uphill or distance of the desired profile.
2. Mutation: cycles are selected from the population, and mutated by connecting 2 vertices in the cycle using a randomized breadth first search.

To evaluate the fitness of a candidate solution, we use dynamic time warping between splines fit to the candidate and desired profiles, implemented `here <https://gitlab.com/Bryce-BG/elena-elevation-based-navigation/blob/master/path_finding/solver.py#L41>`_. 
